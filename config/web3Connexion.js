const Web3 = require("web3");
const config = require("./web3Config");

const metadata = require("./build/contracts/BGToken.json");
const abi = metadata.abi;

const addressacc = config.configWeb3.accAdress;

var web3;
if (typeof web3 !== "undefined") {
  web3 = new Web3(web3.currentProvider);
} else {
  web3 = new Web3(new Web3.providers.HttpProvider(config.configWeb3.url));
}
web3.eth.defaultAccount = addressacc;

var contractInstance = new web3.eth.Contract(
  abi,
  config.configWeb3.contractAdress
);

exports.contractInstance = contractInstance;
