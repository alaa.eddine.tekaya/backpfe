pragma solidity ^0.4.24;

import "./BasicToken.sol";
import "./StandardToken.sol";
import "./Ownable.sol";

contract MeterManagement is BasicToken, Ownable, StandardToken {
    
     event Burn(address indexed burner, uint256 value);
     event Mint(address indexed to, uint256 amount);

     address[] public users;

    //---------------------USERS-STRUCT------------------------------------------
     struct user {
        string email;
        string full_name;
        string password;
        string firebase_hash;
        uint balanceOfMoney;
    }
     mapping(address => user) public oneUserInfo;
         
    //-------------------------REGISTER-USER--------------------------------------
    /**
    * @dev Register user with his fullname and email address.
    * @param _full_name The full name of user.
    * @param _email The email address of user.
    * @param _password The password of the user.
    * @return A boolean that indicates if the operation was successful.
    */
    function registerUser(string memory _email, string memory _full_name , string memory _password) 
    public
    returns(bool){
          require(!checkUserExists(msg.sender));
          oneUserInfo[msg.sender].email=_email;  
          oneUserInfo[msg.sender].full_name=_full_name;
          oneUserInfo[msg.sender].password=_password;
          oneUserInfo[msg.sender].firebase_hash="default";
          oneUserInfo[msg.sender].balanceOfMoney = 0;
          users.push(msg.sender) -1;
          return true;
    }
    
    //-------------------------SIGN-IN-USER--------------------------------------
    /**
    * @dev login user with his email address and password.
    * @param _email The email address of user.
    * @param _password The password of the user.
    * @return A boolean that indicates if the operation was successful.
    */
    function signInUser(string memory _email, string memory _password) 
    public
    view
    returns(bool){
          require(checkUserExists(msg.sender));
          
          if(keccak256(abi.encodePacked(oneUserInfo[msg.sender].email)) == keccak256(abi.encodePacked(_email))&&
          keccak256(abi.encodePacked(oneUserInfo[msg.sender].password)) == keccak256(abi.encodePacked(_password))){
              return true;
          }else{
              return false;
          }
    }
    //--------------------------CHECK-EXISTANCE-OF-USER------------------------------
    /**
    * @dev Check existance of the user in the array users.
    * @param _userToCheckAddress The address of the user.
    * @return A boolean that indicates if the user exists.
    */
    function checkUserExists(address _userToCheckAddress) 
    internal 
    view 
    returns(bool){
        for(uint256 i = 0; i < users.length; i++){
        if(users[i] == _userToCheckAddress) return true;}             
        return false;
    }
    //------------------------------SET-FIREBASE-HASH--------------------------------------
    /**
    * @dev Add firebase hash to user
    * @param _addressToSetFirebase The address of the user.
    * @param _firebase_hash The firebase hash of the user.
    * @return A boolean that indicates if the user exists.
    */
    function setFirebaseHashUser(address _addressToSetFirebase, string memory _firebase_hash)  
    public
    onlyOwner
    returns(bool){
        require(checkUserExists(_addressToSetFirebase));
        oneUserInfo[_addressToSetFirebase].firebase_hash =_firebase_hash;
        return true;
    }
    
    //------------------------GET-ALL-USERS----------------------------------------------------
    /**
    * @dev Function to view all registered users 
    * @return An array of users
    */    
    function getAllUsers()
    public
    view
    returns (address[]){
        return users;
    }
    //----------------------ADD-MONEY-TO-USER---------------------------------------
    function addMoneyToUser(address address_to_add, uint _amount)
    public 
    onlyOwner
    returns(bool){
         require(checkUserExists(address_to_add));
          oneUserInfo[address_to_add].balanceOfMoney += _amount;
          return(true);
    }
    //----------------------REMOVE-MONEY-TO-USER---------------------------------------
    function RemoveMoneyToUser(address address_to_add, uint _amount)
    public 
    onlyOwner
    returns(bool){
         require(checkUserExists(address_to_add));
          oneUserInfo[address_to_add].balanceOfMoney -= _amount;
          return(true);
    }
    
    //-----------------------------GET-USER-FIREBASE-BY-ADDRESS---------------------------------
    function getUserFireHashByAddress(address _addressToGetFirebase)
    public
    view
    returns(string){
         require(checkUserExists(_addressToGetFirebase));
         return(oneUserInfo[_addressToGetFirebase].firebase_hash);
    }
    
//-----------------------------------------METERS----------------------------------------------
    address[] public meters;
    
    //-------------------------METER-STRUCT-----------------------------------------
    struct Meter {
      address owner;
      string longitude;
      string latitude;
      string meter_reference;
      string firebase_hash;
    }
         mapping(address => Meter) public oneMeterInfo;
    
    //--------------------------REGISTER-METER------------------------------------------
    /**
    * @dev Register meter with its params.
    * @return A boolean that indicates if the operation was successful.
    */
    function registerMeter(address _meterAddress, address _ownerAddress, string memory _longitude,
        string memory _latitude, string memory _meter_reference) 
    public
    onlyOwner
    returns(bool){
        require(checkUserExists(_ownerAddress));
        require(!checkMeterExists(_meterAddress));
          
        oneMeterInfo[_meterAddress].owner=_ownerAddress;  
        oneMeterInfo[_meterAddress].longitude=_longitude;
        oneMeterInfo[_meterAddress].latitude=_latitude;
        oneMeterInfo[_meterAddress].meter_reference=_meter_reference;
        oneMeterInfo[_meterAddress].firebase_hash="default";
        meters.push(_meterAddress) -1;
        return true;
    } 

   
    //----------------------CHECK EXISTANCE OF METER----------------------------------------
    function checkMeterExists(address _meterToCheckAddress) 
    internal 
    view 
    returns(bool){
         for(uint256 i = 0; i < meters.length; i++){
         if(meters[i] == _meterToCheckAddress) return true;}             
        return false;
    }
    //--------------------------------------------------------------------------------------
    modifier onlyMeterOwner(address _meterAddress){
    require(oneMeterInfo[_meterAddress].owner == msg.sender);
    _;
    }
     //------------------------------SET-FIREBASE-HASH--------------------------------------
    /**
    * @dev Add firebase hash to user
    * @return A boolean that indicates if the user exists.
    */
    function setFirebaseHashMeter(address _addressToSetFirebase, string memory _firebase_hash)  
    public
    onlyOwner
    returns(bool){
        require(checkMeterExists(_addressToSetFirebase));
        oneMeterInfo[_addressToSetFirebase].firebase_hash =_firebase_hash;
        return true;
    }
    //-----------------------------GET-METER-FIREBASE-BY-ADDRESS---------------------------------
    function getMeterFireHashByAddress(address _addressToGetFirebase)
    public
    view
    returns(string){
         require(checkMeterExists(_addressToGetFirebase));
         return(oneMeterInfo[_addressToGetFirebase].firebase_hash);
    }
    //----------------------TRANSFER-METER-OWNERSHIP----------------------------------------
    /**
    * @dev Allows the owner of the meter to transfer ownership of thier meter to a new address
    * @param _meterAddress The address of the meter.
    * @param _newOwnerAddress The address of the new owner.
    * @return A boolean that indicates if the meter exists.
    */
    function transferMeterOwnership(address _meterAddress, address _newOwnerAddress)
    onlyOwner
    public
    returns(bool)
    {
        oneMeterInfo[_meterAddress].owner = _newOwnerAddress;
        return true;
    }
    //-------------------------------------------------------------------------------------   
    
    modifier hasMintPermission() { // Requires that only the meter address can call the mint function based on production
    require(oneMeterInfo[msg.sender].owner != 0 ||
        msg.sender == owner);
        _;
    }
    //-------------------------------MINT-TOKENS--------------------------------------------
    /**
    * @dev Function to mint tokens 
    * @param _amount The amount of tokens to mint.
    * @return A boolean that indicates if the operation was successful.
    */    
    function mint(uint256 _amount)
    public
    hasMintPermission
    returns (bool)
    {
        totalSupply_ = totalSupply_.add(_amount); // Increases total supply of tokens in circulation during minting event
        balances[msg.sender] = balances[msg.sender].add(_amount); // Increases wallet balance of minting meter upon power production
        increaseApproval(oneMeterInfo[msg.sender].owner , _amount); // Approves the increase in meter's balance during minting
        emit Mint(msg.sender, _amount);
        emit Transfer(address(0), msg.sender, _amount);
        return true;
    }
     
    //------------------------------BURN-TOKENS--------------------------------------------
    /**
    * @dev Burns a specific amount of tokens.
    * @param _value The amount of token to be burned.
    */
    function burn(uint256 _value) public { // Burn tokens from the meter's own wallet [msg.sender]
        _burn(msg.sender, _value);
    }
    function _burn(address _who, uint256 _value) internal {
        require(_value <= balances[_who]);
        balances[_who] = balances[_who].sub(_value);
        totalSupply_ = totalSupply_.sub(_value); // Reduce total supply of tokens in circulation during burn event
        emit Burn(_who, _value);
        emit Transfer(_who, address(0), _value);
    }
     
    //-------------------------MINT-TOKEN-TO-ADDRESS-----------------------------------------
    /**
    * @dev Function to mint tokens to an address, as an owner
    * @param _amount of tokens to mint.
    * @param _recipient address of the minting process
    * @return A boolean that indicates if the operation was successful.
    */    
    function mintTo(uint256 _amount, address _recipient)
    public
    onlyOwner
    returns (bool)
    {
        totalSupply_ = totalSupply_.add(_amount); // Increases total supply of tokens in circulation during minting event
        balances[_recipient] = balances[_recipient].add(_amount); // Increases wallet balance of minting meter upon power production
        increaseApproval(oneMeterInfo[msg.sender].owner, _amount); // Approves the increase in meter's balance during minting
        emit Mint(_recipient, _amount);
        emit Transfer(address(0), _recipient, _amount);
        return true;
    }
    //-----------------------------GET-ALL-METERS------------------------------------------
    /**
    * @dev Function to view all registered meters 
    * @return An array of meters
    */    
    function getAllMeters()
    public
    view
    returns (address[]){
        return meters;
    }
   
//-----------------------------------DISPATCHERS-----------------------------------------------------------
    address[] public dispatchers;
    //---------------------------------DISPATCHER-STRUCT---------------------------------------
    struct Dispatcher {
      string dispatcher_reference;
      string longitude;
      string latitude;
      string firebase_hash;
    }
         mapping(address => Dispatcher) public oneDispatcherInfo;
         
  //----------------------------------REGISTER-DISPATCHER--------------------------------------
    /**
    * @dev Register dispatcher with his its params
    * @return A boolean that indicates if the operation was successful.
    */
    function registerDispatcher(address _dispatcher_address, string memory _dispatcher_reference, 
        string memory _longitude , string memory _latitude) 
    public
    onlyOwner
    returns(bool){
      require(!checkDispatcherExists(_dispatcher_address));
      oneDispatcherInfo[_dispatcher_address].dispatcher_reference=_dispatcher_reference;
      oneDispatcherInfo[_dispatcher_address].longitude=_longitude; 
      oneDispatcherInfo[_dispatcher_address].latitude=_latitude; 
      oneDispatcherInfo[_dispatcher_address].firebase_hash="default";
      dispatchers.push(_dispatcher_address) -1;
      return true;
    }
     //---------------------SET-FIREBASE-HASH-OF-DISPATCHER--------------------------------------
    function setFirebaseHashDispatcher(address _addressToSetFirebase, string memory _firebase_hash)  
    public
    onlyOwner
    returns(bool){
       require(checkDispatcherExists(_addressToSetFirebase));
        oneDispatcherInfo[_addressToSetFirebase].firebase_hash =_firebase_hash;
        return true;
    }
    //----------------------CHECK EXISTANCE OF DISPATCHER----------------------------------------
    function checkDispatcherExists(address _dispatcher_to_check_address) 
    internal 
    view
    returns(bool){
        for(uint256 i = 0; i < dispatchers.length; i++){
        if(dispatchers[i] == _dispatcher_to_check_address) return true;}             
        return false;
    }
    //-------------------------------GET-ALL-DISPATCHERS---------------------------------------------
    function getAllDispatchers()
    public
    view
    returns (address[]){
        return dispatchers;
    }
    //-----------------------------GET-DISPATCHER-FIREBASE-BY-ADDRESS---------------------------------
    function getDispatcherFireHashByAddress(address _addressToGetFirebase)
    public
    view
    returns(string){
         require(checkDispatcherExists(_addressToGetFirebase));
         return(oneDispatcherInfo[_addressToGetFirebase].firebase_hash);
    }
        


}