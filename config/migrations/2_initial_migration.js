const BasicToken = artifacts.require("../contracts/BasicToken.sol");
const BGToken = artifacts.require("../contracts/BGToken.sol");
const ERC20 = artifacts.require("../contracts/ERC20.sol");
const ERC20Basic = artifacts.require("../contracts/ERC20Basic.sol");
const MeterManagement = artifacts.require("../contracts/MeterManagement.sol");
const Migrations = artifacts.require("../contracts/Migrations.sol");
const Ownable = artifacts.require("../contracts/Ownable.sol");
const SafeMath = artifacts.require("../contracts/SafeMath.sol");
const StandardToken = artifacts.require("../contracts/StandardToken.sol");

module.exports = function(deployer) {
  //deployer.deploy(BasicToken);
  deployer.deploy(BGToken);
  
  
  //deployer.deploy(MeterManagement);
  //deployer.deploy(Migrations);
  //deployer.deploy(Ownable);
  //deployer.deploy(SafeMath);
  //deployer.deploy(StandardToken);

};
