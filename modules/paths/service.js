const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");
const pathDao = require("../../dao/pathDao");

module.exports = {
  addPath: function(path, cb) {
    pathDao.add(path, r => {
      cb(r);
    });
  },

  updateStat: function(pathId, val, cb) {
    pathDao.setIsActive(pathId, val, r => {
      cb(r);
    });
  },
  getAll: function(cb) {
    pathDao.findAll(r => {
      cb(r);
    });
  }
};
