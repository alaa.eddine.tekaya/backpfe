const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const service = require("./service");

router.get("/test", (req, res) => {
  res.send("path test");
});
//---------------------------------------add path -------------------------------------
//service route : /addpath
//parameters send in body
//parameters name:
// points : points of the path you want to add
router.post(routing.ADDPATH, async (req, res) => {
  try {
    var path = {
      active: 1,
      points: "0,0/0,0"
    };

    path.points = req.body.points;
    service.addPath(path, ok => {
      if (ok) {
        res.send("path added succesfully !!");
      } else {
        res.send("failed to add path !!");
      }
    });
  } catch (e) {
    console.log(e);
    res.send(e);
  }
});

//---------------------------------------Set active -------------------------------------
//service route : /setactive
//parameters send in body
//parameters name:
// pathId : firebase id of the path
// val : 0/1 to set if a path is active or not
router.post(routing.SETACTIVE, async (req, res) => {
  try {
    service.updateStat(req.body.pathId, req.body.val, ok => {
      if (ok) {
        res.send("path updated succesfully !!!");
      } else {
        res.send("failed to update path !!");
      }
    });
  } catch (e) {
    console.log(e);
    res.send(e);
  }
});

//----------------------------------get all paths-----------------------------------
//service route /getpaths
//no parameters
router.get(routing.GETPATHS, async (req, res) => {
  service.getAll(r => {
    if (r != "error") {
      res.send(r);
    } else {
      res.send("failed to get paths");
    }
  });
});

module.exports = router;
