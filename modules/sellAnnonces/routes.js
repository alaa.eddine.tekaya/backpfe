const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const service = require("./service");
const controler = require("./controller");
router.get("/test", (req, res) => {
  res.send("sell announces test ");
});
//---------------------------add sell announces--------------------------------
//service route : /addsell
//parameters :
/*
    meterAddress
    energyType
    startDate
    endDate 
    price
    */
router.post(routing.ADDSELL, async (req, res) => {
  var annonce = {
    meterAddress: "",
    owner: "",
    energyType: "",
    startDate: "",
    endDate: "",
    price: "",
    status: "actif",
    meterId: ""
  };
  annonce.meterAddress = req.body.meterAddress;
  annonce.energyType = req.body.energyType;
  annonce.startDate = req.body.startDate;
  annonce.endDate = req.body.endDate;
  annonce.price = req.body.price;
  controler.addAnnounce(annonce, ok => {
    if (ok == "done") {
      res.send("added perfectly !!");
    }

    if (ok == "error") {
      res.send("no such meter with that id");
    }

    if (ok == "exist") {
      res.send("meter has already announce");
    }
  });
});
//note check urls may have change : /getannounces become for get all announces
//---------------------------get announces BY USER --------------------------------
//service route : /getannouncesByUser
//in url
//parameters :
//userAddress :
router.get(routing.GET_ANNOUNCESBYUSER, async (req, res) => {
  if (req.query.userAddress != null) {
    service.getMyAnnonces(req.query.userAddress, r => {
      if (r != "error") {
        res.send(r);
      } else {
        res.send("error to get annonce ");
      }
    });
  } else {
    res.send("meterId is undefined : no meterId is given in parameter  ");
  }
});
//-------------------------get All anounces -----------------------------------------
//service route /getannounces

//http://localhost:3000/api/sellAnnonces/getannounces

router.get(routing.GET_ANNOUNCES, async (req, res) => {
  service.getAll(r => {
    res.send(r);
  });
});
//---------------------------delete my announces--------------------------------
//service route : /delete
//parameters :
//announceId :
router.delete(routing.DELETE, (req, res) => {
  service.deleteMyAnnonce(req.body.announceId, r => {
    if (r != "error") {
      res.send("announce deleted !!");
    } else {
      res.send("failed to deleted");
    }
  });
});
//---------------------------update my announce state--------------------------------
//service route : /setstate
//parameters :
//announceId :
//state
router.delete(routing.SET_STATE, (req, res) => {
  service.setState(req.body.announceId, req.body.state, r => {
    if (r != "error") {
      res.send("state updated !!");
    } else {
      res.send("failed to update state");
    }
  });
});

module.exports = router;
