const express = require("express");
const router = express.Router();

const service = require("./service");
const meterDao = require("../../dao/meterDao");
const userDao = require("../../dao/usersDao");

module.exports = {
  addAnnounce: async function(annonce, cb) {
    meterDao.findByAddress(annonce.meterAddress, r => {
      meterDao.findById(Object.keys(r)[0], meter => {
        if (meter != null) {
          var owner = meter.owner;
          annonce.owner = owner;
          annonce.meterId = Object.keys(r)[0];
          service.addAnnonce(annonce, r => {
            cb(r);
          });
        } else {
          cb("error");
          console.log("no meter has such id");
        }
      });
    });
  }
};
