const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");
const sellAnnonceDao = require("../../dao/sellAnnoncesDao");

module.exports = {
  addAnnonce: async function(annonce, cb) {
    await sellAnnonceDao.findByMeter(annonce.meterAddress, r => {
      if (r == null) {
        sellAnnonceDao.add(annonce, ok => {});
        cb("done");
      } else {
        cb("exist");
      }
    });
  },
  getMyAnnonces: async function(useradd, cb) {
    sellAnnonceDao.findByUser(useradd, r => {
      cb(r);
    });
  },

  getAll: async function(cb) {
    sellAnnonceDao.findAll(r => {
      cb(r);
    });
  },
  deleteMyAnnonce: async function(announceId, cb) {
    sellAnnonceDao.Delete(announceId, r => {
      cb(r);
    });
  },
  setState: async function(announceId, state, cb) {
    sellAnnonceDao.updateState(announceId, state, r => {
      cb(r);
    });
  }
};
