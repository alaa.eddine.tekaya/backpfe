const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");
const userDao = require("../../dao/usersDao");
const cardDao = require("../../dao/cardDao");
module.exports = {
  //------------------------------ADDMoney smart contract----------------------------------------//
  //parameters :
  //@address : address of the seller in our case
  //@amount : amount of money in DT to add
  addMoney_sc: async function(address, amount, cb) {
    var ok;
    await contract.contractInstance.methods
      .addMoneyToUser(address, amount)
      .send({ from: config.configWeb3.accAdress, gas: config.configWeb3.gaz })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok, receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },

  //------------------------------RemoveMoney smart contract----------------------------------------//

  //parameters :
  //@address : address of the seller in our case
  //@amount : amount of money in DT to add
  removeMoney_sc: async function(address, amount, cb) {
    var ok;
    await contract.contractInstance.methods
      .RemoveMoneyToUser(address, amount)
      .send({ from: config.configWeb3.accAdress, gas: config.configWeb3.gaz })
      .on("error", error => {
        ok = false;
        console.log("probleeeeem remove money !!");
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok, receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },

  //-------------- database  functions to update balance

  AddBalance: function(amount, address) {
    userDao.findByAddress(address, r => {
      userDao.updateBalance(
        Object.keys(r)[0],
        parseInt(r[Object.keys(r)[0]].balanceDt) + parseInt(amount)
      );
    });
  },
  removeBalance: function(amount, address) {
    userDao.findByAddress(address, r => {
      userDao.updateBalance(
        Object.keys(r)[0],
        parseInt(r[Object.keys(r)[0]].balanceDt) - parseInt(amount)
      );
    });
  },

  addCard: function(userAddress, card, cb) {
    cardDao.addCard(userAddress, card, ok => {
      cb(ok);
    });
  }
};
