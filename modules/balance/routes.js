const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const control = require("./controller");
const service = require("./service");

router.get("/test", (req, res) => {
  res.send("testtessst");
});
//add money from your visa card
//routes : /addmoney
//parameters in body :
//req.body.userAddress
//req.body.amount
// req.body.cardholder
// req.body.cardnumbe
// req.body.expirydate
// req.body.cardType

router.post(routing.ADDMONEY, (req, res) => {
  var card = {
    balanceDt: 5000,
    Expirydate: "",
    cardholder: "",
    cardnumber: "",
    cardtype: "",
    cvv: ""
  };
  card.cardholder = req.body.cardholder;
  card.cardnumber = req.body.cardnumber;
  card.Expirydate = req.body.expirydate;
  card.cardtype = req.body.cardType;
  card.cvv = req.body.cvv;
  control.AddMoney(req.body.userAddress, card, req.body.amount, ok => {
    if (ok) {
      res.send("money Add it ");
    } else {
      res.send("failed to Add money ");
    }
  });
});

//--------------------------------------Add card , you can only add one --------------------------------------
//route : /addcard
//parameters send in body request
//parameters :
//req.body.userAddress
// req.body.cardholder
// req.body.cardnumbe
// req.body.expirydate
// req.body.cardType

// req.body.cvv;

router.post(routing.ADDCARD, async (req, res) => {
  var card = {
    balanceDt: 5000,
    Expirydate: "",
    cardholder: "",
    cardnumber: "",
    cardtype: "",
    cvv: ""
  };
  try {
    card.cardholder = req.body.cardholder;
    card.cardnumber = req.body.cardnumber;
    card.Expirydate = req.body.expirydate;
    card.cardtype = req.body.cardType;
    card.cvv = req.body.cvv;
    service.addCard(req.body.userAddress, card, ok => {
      if (ok) {
        res.send("done");
      } else {
        res.send("failed");
      }
    });
  } catch (e) {
    res.status(404);
    console.log(e);
  }
});

module.exports = router;
