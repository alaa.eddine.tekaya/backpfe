const express = require("express");
const router = express.Router();

const service = require("./service");
const cardDao = require("../../dao/cardDao");
const userDao = require("../../dao/usersDao");

module.exports = {
  //when we get buy request , we add the price to the seller balance in the smart contract and the database
  //and the same we remove the price from buyer balance

  AddMoney: function(address, card, amount, cb) {
    cardDao.findByAddress(address, r => {
      if (
        r.cardholder == card.cardholder &&
        r.cardnumber == card.cardnumber &&
        r.cardtype == card.cardtype &&
        r.cvv == card.cvv &&
        r.balanceDt >= amount
      ) {
        cardDao.updateBalance(
          address,
          parseInt(r.balanceDt) - parseInt(amount)
        );
        service.AddBalance(amount, address);
        cb(true);
      } else {
        cb(false);
      }
    });
  }
};
