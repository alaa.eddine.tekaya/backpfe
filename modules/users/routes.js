const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const service = require("./service");

router.get("/test", (req, res) => {
  res.send("user test");
});
//get all users from smart contract
//service route : /getUsersSc
//without parameters
router.get(routing.GET_USERS_SC, async (req, res) => {
  service.getAllFromSC(r => {
    res.send(r);
  });
});

//get all users  from DB
//service route : /getusers
//parameters :

router.get(routing.GET_USERS, async (req, res) => {
  service.getAll(r => {
    if (r != null) {
      res.send(r);
    } else {
      res.send("no users");
    }
  });
});

//get firebase id by address from smart contract
//service route : /getUserFirebaseIdSc
//parameters :
// add : user address
router.get(routing.GET_USERFIREBASEID_SC, async (req, res) => {
  service.getFirebaseId(req.query.add, r => {
    res.send(r);
  });
});

//get user by address from DB
//service route : /getUserByAdd
//parameters :
// add : user address
router.get(routing.GET_USERBYADD, async (req, res) => {
  service.getByAddress(req.query.add, r => {
    if (r != null) {
      res.send(r);
    } else {
      res.send("no user match this address");
    }
  });
});

//get user by firebase id from DB
//service route : /getUserById
//parameters :
// id : user firebase id
router.get(routing.GET_USERBYID, async (req, res) => {
  service.getById(req.query.id, r => {
    res.send(r);
  });
});

//get user transaction by wallet address  from DB
//service route : /getTransacByAdd
//parameters :
// address : user address
router.get(routing.GET_TRANSACTIONSBYADD, async (req, res) => {
  service.getTransactionByAdd(req.query.address, r => {
    if (r != null) {
      res.send(r);
    } else {
      res.send("vide");
    }
  });
});

//set a physicall address by firebase id
//service route : /setPhysicalAdd
//parameters :
// id : user firebase id
/*
    addressline1  : 
    addressline2  :
    city          :
    country       : 
    state         :
    type          :
    zip           :
*/
router.post(routing.SET_PHYSICALADD, async (req, res) => {
  phyAdd = {
    addressline1: "",
    addressline2: "",
    city: "",
    country: "",
    state: "",
    type: "",
    zip: ""
  };
  phyAdd.addressline1 = req.body.addressline1;
  phyAdd.addressline2 = req.body.addressline2;
  phyAdd.city = req.body.city;
  phyAdd.country = req.body.country;
  phyAdd.state = req.body.state;
  phyAdd.type = req.body.type;
  phyAdd.zip = req.body.zip;

  service.updatePhAdd(req.body.id, phyAdd);
  res.send("updated");
});

//service route : /setRate
//parameters :
// address : user address
// rate : rate average
router.post(routing.SET_RATE, async (req, res) => {
  service.updateRating(req.body.address, req.body.rate, ok => {
    if (ok != "null") {
      res.send("updated");
    } else {
      res.send("address not found ");
    }
  });
});

//service route : /setproducer
//parameters :
// address : user address
// producer : producer
router.post(routing.SET_PRODUCER, async (req, res) => {
  service.setProducer(req.body.address, req.body.producer, ok => {
    if (ok == "done") {
      res.send("updated");
    } else {
      res.send("address not found ");
    }
  });
});

module.exports = router;
