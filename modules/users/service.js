const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");
const userDao = require("../../dao/usersDao");

module.exports = {
  //------------------------------------get all user from smart contract-------------------------------//
  getAllFromSC: async function(cb) {
    await contract.contractInstance.methods
      .getAllUsers()
      .call({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      });
  },
  //---------------------------------------get firebase id from smart contract--------------------------//
  //@address user wallet address

  getFirebaseId: async function(address, cb) {
    await contract.contractInstance.methods
      .getUserFireHashByAddress(address)
      .call({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      });
  },
  //----------------------------------------get user user BlanceDt from smart contract using wallet address------------------------
  getBalance: async function(address, cb) {
    await contract.contractInstance.methods
      .balanceOf(address)
      .call({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      });
  },

  //----------------------------------------get user from DB by wallet address------------------------

  getByAddress: async function(address, cb) {
    userDao.findByAddress(address, r => {
      cb(r);
    });
  },
  //----------------------------------------get all user from DB ------------------------

  getAll: async function(cb) {
    userDao.findAll(r => {
      cb(r);
    });
  },

  //----------------------------------------get user from DB by firebase id ---------------------------
  getById: async function(id, cb) {
    userDao.findById(id, r => {
      cb(r);
    });
  },
  //----------------------------------------get transactions from DB by firebase address ---------------------------
  getTransactionByAdd: async function(address, cb) {
    userDao.findByAddress(address, r => {
      if (r != null) {
        cb(r[Object.keys(r)[0]].transactions);
      } else {
        cb("null");
      }
    });
  },

  //----------------------------------------update user physicall address in DB by firebase id ---------------------------

  updatePhAdd: async function(id, physicalAdd) {
    userDao.updatePhAddress(id, physicalAdd);
  },
  //----------------------------------------update user rate in DB by firebase id ---------------------------

  updateRating: async function(address, rate, cb) {
    userDao.findByAddress(address, r => {
      if (r != null) {
        var moyenRate = (r[Object.keys(r)[0]].rating + rate) / 2;
        userDao.updateRate(Object.keys(r)[0], moyenRate);
      } else {
        cb("null");
      }
    });
  },
  //----------------------------------------set producer  in DB by address ---------------------------

  setProducer: async function(address, producer, cb) {
    userDao.findByAddress(address, r => {
      if (r != null) {
        userDao.updateProducer(Object.keys(r)[0], producer);
        cb("done");
      } else {
        cb("null");
      }
    });
  }
};
