const express = require("express");
const router = express.Router();
const axios = require("axios");
const service = require("./service");
const meterDao = require("../../dao/meterDao");
const userDao = require("../../dao/usersDao");

module.exports = {
  generalRegestration: async function(meter, cb) {
    try {
      await service.registerMeter(
        meter.address,
        meter.owner,
        meter.long,
        meter.lat,
        meter.meterReference,
        ok => {
          cb(ok);
          if (ok) {
            userDao.findByAddress(meter.owner, r => {
              if (r != null) {
                userDao.addMeter(Object.keys(r)[0], meter.meterReference, r => {
                  meter.RefFirebaseId = r;
                  meterDao.add(meter);
                });
              }
            });
          }
        }
      );

      function setFire() {
        meterDao.findByAddress(meter.address, async r => {
          if (r != null) {
            await service.setFirebaseId(meter.address, Object.keys(r)[0]);
            axios({
              method: "get",
              url:
                "https://nominatim.openstreetmap.org/reverse?format=json&lat=" +
                meter.long +
                "&lon=" +
                meter.lat +
                "&zoom=18&addressdetails=1",
              responseType: "json"
            }).then(function(response) {
              meterDao.setAddress(Object.keys(r)[0], response.data.address);
            });
          } else {
            console.log("!!");
          }
        });
      }
      setTimeout(setFire, 1000);
    } catch (err) {
      console.log("from meter controler " + err);
    }
  },

  generalOwnerSet: async function(meterAdd, newOwner, cb) {
    try {
      service.setMeterOwner(meterAdd, newOwner, async ok => {
        if (ok) {
          await meterDao.findByAddress(meterAdd, async meter => {
            if (meter != null) {
              await userDao.findByAddress(
                meter[Object.keys(meter)[0]].owner,
                user => {
                  console.log(
                    "delete\n" +
                      meter[Object.keys(meter)[0]].RefFirebaseId +
                      "from :" +
                      Object.keys(user)[0]
                  );
                  userDao.deleteByMeter(
                    Object.keys(user)[0],
                    meter[Object.keys(meter)[0]].RefFirebaseId
                  );
                }
              );
              userDao.findByAddress(newOwner, user => {
                console.log("add meter\n");
                userDao.addMeter(
                  Object.keys(user)[0],
                  meter[Object.keys(meter)[0]].meterReference,
                  id => {
                    meterDao.setRefFirebaseId(Object.keys(meter)[0], id);
                  }
                );
              });
              console.log("set owner\n");
              meterDao.setOwner(Object.keys(meter)[0], newOwner);
            } else {
              ok = false;
            }
          });
          cb(ok);
        }
      });
    } catch (err) {
      console.log("from meter controler " + err);
    }
  }
};
