const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const service = require("./service");
const control = require("./controller");

router.get("/test", (req, res) => {
  res.send("meter test");
});

//---------------------------------------add meter -------------------------------------

//service route : /addmeter
//parameters are sent in body
//parameters name :
// meterAddress : meter etheureum address
//userAdd : user etheureum address (meter owner)
//meterReference
//long : meter position longitude
//lat :   meter position latitude
//producerCapacity
router.post(routing.ADD_METER, async (req, res) => {
  var meter = {
    producerCapacity: "",
    isActive: 1,
    lat: "",
    long: "",
    meterReference: "0",
    owner: "",
    address: "",
    realTime: { consum: "0", prod: "0" },
    totalConsumption: "0",
    totalProduction: "0",
    transactions: {},
    RefFirebaseId: "none",
    connexionCount: 0
  };
  meter.address = req.body.meterAddress;
  meter.owner = req.body.userAdd;
  meter.meterReference = req.body.meterReference;
  meter.long = req.body.long;
  meter.lat = req.body.lat;
  meter.producerCapacity = req.body.producerCapacity;

  control.generalRegestration(meter, ok => {
    if (ok) {
      res.send("meter registration done!!");
    } else {
      res.send("meter exist or owner doenst exist!!");
    }
  });
});

//--------------------------------------Set owner--------------------------------------
//route : /setowner
//parameters send in body request
//parameters :
//meterAdd
//newOwner
router.post(routing.SET_METEROWNER, async (req, res) => {
  try {
    control.generalOwnerSet(req.body.meterAdd, req.body.newOwner, ok => {
      if (ok) {
        res.send("set owner done!!");
      } else {
        res.send("set owner failed !!");
      }
    });
  } catch (e) {
    console.log(e);
  }
});

//get firebase id by address from smart contract
//route /getMeterIdSc
//parameters are sent in the url not body
//example : http://localhost:3000/api/meter/getMeterIdSc?add=0x365ca4bbca091d8b30ea9a79826f93f22245cedf
//parameters :
//add : meter address
router.get(routing.GET_METERFIREBASEID_SC, async (req, res) => {
  service.getFirebaseId(req.query.add, r => {
    if (r != "error") {
      res.send(r);
    } else {
      res.send("nothing");
    }
  });
});

//get firebase id by address from smart contract
//route /getbalance
//parameters are sent in the url not body
//example : http://localhost:3000/api/meter/getbalance?add=0x365ca4bbca091d8b30ea9a79826f93f22245cedf
//parameters :
//add : meter address
router.get(routing.GET_BALANCE, async (req, res) => {
  service.getBalance(req.query.meterAddress, r => {
    if (r != "error") {
      res.send(r);
    } else {
      res.send("failed to get BGTOKEN balance");
    }
  });
});

//--------------------------------get realtime data from DB--------------------------------------
//service route /getrtdata
//parameters send in url example :
//http://localhost:3000/api/meter/getrtdata?meterId=ffffff
//parameters :
//meterId : firebase id of the meter
router.get(routing.GET_REALTIMEDATA, async (req, res) => {
  service.getById(req.query.meterId, r => {
    res.send(r.realTime);
  });
});

//-------------------------get meter by firebase id from DB-----------------------------------------
//service route /getmeterbyid
//parameters send in url example :
//http://localhost:3000/api/meter/getmeterbyid?meterId=ffffff
//parameters :
//meterId : firebase id of the meter
router.get(routing.GET_METERBYID, async (req, res) => {
  service.getById(req.query.meterId, r => {
    res.send(r);
  });
});

//------------------------------get meters by user from DB-----------------------------------
//service route /getmeterbyowner
//parameters send in url example :
//http://localhost:3000/api/meter/getmeterbyowner?owner=0x4sfdsf4s51fsdf1sbgfhf1dgdfnhf
//parameters :
//owner : user wallet address
router.get(routing.GET_METERBYOWNER, async (req, res) => {
  service.getByOwner(req.query.owner, r => {
    res.send(r);
  });
});

//-------------------------get All meters from DB-----------------------------------------
//service route /getmeters

//http://localhost:3000/api/meter/getmeters

router.get(routing.GET_METERS, async (req, res) => {
  service.getAll(r => {
    res.send(r);
  });
});

//set realtime data
//service route : /setrtdata
//parameters send in body
//id : meter firebase id
/*realTime : json object {
                           consum:
                           prod:

                          }
  */
router.post(routing.SET_REALTIMEDATA, async (req, res) => {
  service.setRealTime(req.body.id, req.body.realTime);
  res.status(200);
});

//set is Active
//service route : /setisactive
//parameters send in body
//id : meter firebase id
//val : 0/1

router.post(routing.SET_ISACTIVE, async (req, res) => {
  service.setIsActive(req.body.id, req.body.val);
  if (parseInt(req.body.val)) {
    res.send("meter is activated  ");
  } else {
    res.send("meter is desactivated ");
  }
});

//set  total production
//service route : /setrtdata
//parameters send in body
// meterAddress : meter address
//totalProd :  total production
router.post(routing.SET_TOTPROD, async (req, res) => {
  service.setTotProd(req.body.meterAddress, req.body.totalProd);
  res.status(200);
});

//set total consumtion
//set  total production
//service route : /setrtdata
//parameters send in body
//meterAddress : meter address
//totalconsum : total consumption
router.post(routing.SET_TOTCONS, async (req, res) => {
  service.setTotCons(req.body.meterAddress, req.body.totalconsum);
  res.status(200);
});

//---------------------------------------------connexions---------------------------------------------

//--------------------------------------Add connexion--------------------------------------
//route : /addconnexion
//parameters send in body request
//parameters :
//meterAddress
//connexion : format :
// { path : "",
//   dispatcher : ""
// }

router.post(routing.ADDCONNEXION, async (req, res) => {
  try {
    service.addConnexions(req.body.meterAddress, req.body.connexion);
    res.send("done");
    res.status(200);
  } catch (e) {
    res.status(404);
    console.log(e);
  }
});

//---------------------------delete connexion by key (1,2,3....)--------------------------------
//service route : /deleteconn
//parameters :
//connexion num : in get all connexion i will send you an array
//so if you want to delete the first in the array just set num 1
router.delete(routing.DELETECONN, (req, res) => {
  service.deleteConnexion(req.body.meterAddress, req.body.num, r => {
    if (r) {
      res.send("deleted");
    } else {
      res.send("failed");
    }
  });
});
//-------------------------get All connexions of meter -----------------------------------------
//service route /getconnexions

//http://localhost:3000/api/meter/getconnexions?meterAddress=0xddjfkdskfsmldflksd

router.get(routing.GET_CONNEXIONS, async (req, res) => {
  service.getConnexions(req.query.meterAddress, r => {
    res.send(r);
  });
});

module.exports = router;
