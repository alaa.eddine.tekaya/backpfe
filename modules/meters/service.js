const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");
const meterDao = require("../../dao/meterDao");

module.exports = {
  //register meter in smart contract
  registerMeter: async function(meterAdd, userAdd, log, lat, ref, cb) {
    var ok;
    await contract.contractInstance.methods
      .registerMeter(meterAdd, userAdd, log, lat, ref)
      .send({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok);
        console.log(receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },
  addConnexions: async function(meteraddress, connexion) {
    meterDao.findByAddress(meteraddress, meter => {
      var key = meter[Object.keys(meter)[0]].connexionCount + 1;
      var id = Object.keys(meter)[0];
      meterDao.addConnexion(id, connexion, key);
      meterDao.updateConnexionCount(id, key);
    });
  },
  deleteConnexion: async function(meterAddress, key, cb) {
    meterDao.findByAddress(meterAddress, meter => {
      if (meter[Object.keys(meter)[0]].connexions[key] != null) {
        meterDao.DeleteConnexion(Object.keys(meter)[0], key, r => {
          meterDao.updateConnexionCount(
            Object.keys(meter)[0],
            meter[Object.keys(meter)[0]].connexionCount - 1
          );
          cb(r);
        });
      } else {
        cb(false);
      }
    });
  },
  getConnexions: async function(meterAddress, cb) {
    meterDao.findByAddress(meterAddress, async meter => {
      var connexions = [];
      for (
        let key = 1;
        key < meter[Object.keys(meter)[0]].connexionCount + 1;
        key++
      ) {
        connexions.push(meter[Object.keys(meter)[0]].connexions[key]);
      }
      cb(connexions);
    });
  },

  //get meter firebase id from smart contract
  getFirebaseId: async function(add, cb) {
    await contract.contractInstance.methods
      .getMeterFireHashByAddress(add)
      .call({
        from: add,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      })
      .catch(err => {
        cb("error");
      });
  },
  //get all meters from DB
  getAll: async function(cb) {
    meterDao.findAll(r => {
      cb(r);
    });
  },
  //get meters by user from DB
  getByOwner: async function(userAddress, cb) {
    meterDao.findByUser(userAddress, r => {
      cb(r);
    });
  },
  //get meter by id from DB
  getById: async function(id, cb) {
    meterDao.findById(id, r => {
      cb(r);
    });
  },
  getBalance: async function(add, cb) {
    await contract.contractInstance.methods
      .balanceOf(add)
      .call({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      })
      .catch(err => {
        cb("error");
      });
  },

  setFirebaseId: async function(add, id) {
    await contract.contractInstance.methods
      .setFirebaseHashMeter(add, id)
      .send({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .on("error", error => {
        console.log(error);
      })
      .on("receipt", receipt => {
        console.log(receipt);
      })
      .catch(err => {
        console.log("error in setFirebaseId");
      });
  },

  setMeterOwner: async function(meterAdd, newOwner, cb) {
    var ok;
    await contract.contractInstance.methods
      .transferMeterOwnership(meterAdd, newOwner)
      .send({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok);
        console.log(receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },

  setRealTime: async function(id, realTime) {
    meterDao.updateRealTime(id, realTime);
  },
  setIsActive: async function(id, val) {
    meterDao.setIsActive(id, val);
  },
  setTotProd: async function(address, totprod) {
    meterDao.findByAddress(userAddress, r => {
      var tot = meter[Object.keys(meter)[0]].totalProduction + totprod;
      meterDao.updateTotProd(Object.keys(meter)[0], tot);
    });
  },
  setTotCons: async function(id, totcons) {
    meterDao.findByAddress(userAddress, r => {
      var tot = meter[Object.keys(meter)[0]].totalConsumption + totcons;
      meterDao.updateTotCon(Object.keys(meter)[0], tot);
    });
  }
};
