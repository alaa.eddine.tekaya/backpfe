const express = require("express");
const router = express.Router();
const modules = require("../routes.json");
router.use(modules.AUTHETIF_MODULE, require("./authentification"));
router.use(modules.SELLANNONCES_MODULE, require("./sellAnnonces"));
router.use(modules.USER_MODULE, require("./users"));
router.use(modules.METER_MODULE, require("./meters"));
router.use(modules.DISPATCHER_MODULE, require("./dispatchers"));
router.use(modules.BALANCE_MODULE, require("./balance"));
router.use(modules.PATH_MODULE, require("./paths"));
router.use(modules.BGTOKEN_MODULE, require("./BGToken"));

module.exports = router;
