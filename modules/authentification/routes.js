const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const config = require("../../config/web3Config");
const control = require("./controller");
const service = require("./service");
const userDao = require("../../dao/usersDao");
router.get("/test", (req, res) => {
  res.send("testtessst");
});
//regiter api
router.post(routing.REGISTER_USER, async (req, res) => {
  try {
    var user = {
      email: "",
      meterReferences: {},
      transactions: { 0: "" },
      transactionCount: 0,
      physicalAddress: {
        addressline1: "",
        addressline2: "",
        city: "",
        country: "",
        state: "",
        type: "",
        zip: ""
      },
      rating: "0",
      userName: "",
      email: "",
      walletAddress: "",
      balanceDt: "0",
      producer: "none"
    };
    user.userName = req.body.fullName;
    user.email = req.body.email;
    user.walletAddress = req.body.add;

    control.generalRegestration(user, req.body.pwd, r => {
      if (r) {
        res.send("registration done !! ");
      } else {
        res.send("user already exist!!");
      }
    });
  } catch (e) {
    console.log(e);
  }
});
//sign in
router.post(routing.SIGNIN, async (req, res) => {
  try {
    service.signInUser(req.body.email, req.body.pwd, req.body.add, r => {
      if (r) {
        if (req.body.add == config.configWeb3.accAdress) {
          res.send("admin");
        } else {
          res.send("user");
        }
      } else res.send(r);
    });
  } catch (e) {
    console.log(e);
    res.status(502);
  }
});

module.exports = router;
