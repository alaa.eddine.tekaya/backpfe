const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");

module.exports = {
  registerUse: async function(email, name, pwd, add, cb) {
    var ok;
    await contract.contractInstance.methods
      .registerUser(email, name, pwd)
      .send({ from: add, gas: config.configWeb3.gaz })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok);
        console.log(receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },

  signInUser: async function(email, pwd, add, cb) {
    await contract.contractInstance.methods
      .signInUser(email, pwd)
      .call({
        from: add,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      });
  },

  setFirebaseId: async function(add, id) {
    await contract.contractInstance.methods
      .setFirebaseHashUser(add, id)
      .send({ from: config.configWeb3.accAdress, gas: config.configWeb3.gaz })
      .on("error", error => {
        console.log(error);
      })
      .on("receipt", receipt => {
        console.log(receipt);
      })
      .catch(err => {
        console.log("error in setFirebaseId");
      });
  }
};
