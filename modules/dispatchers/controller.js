const express = require("express");
const router = express.Router();

const service = require("./service");
const meterDao = require("../../dao/meterDao");
const dispatcherDao = require("../../dao/dispatcherDao");

module.exports = {
  generalRegestration: async function(dispatcher, cb) {
    try {
      await service.registerDispatcher(
        dispatcher.address,
        dispatcher.ref,
        dispatcher.long,
        dispatcher.lat,

        ok => {
          cb(ok);
          if (ok) {
            dispatcherDao.add(dispatcher);
          }
        }
      );

      dispatcherDao.findByAddress(dispatcher.address, r => {
        if (r != null) {
          service.setFirebaseId(dispatcher.address, Object.keys(r)[0]);
        } else {
          console.log("dispatcher registration failed !!");
        }
      });
    } catch (err) {
      console.log("from dispatcher controler " + err);
    }
  }
};
