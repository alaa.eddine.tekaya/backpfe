const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");
const dispatcherDao = require("../../dao/dispatcherDao");

module.exports = {
  //register meter in smart contract
  registerDispatcher: async function(
    dispatcherAdd,
    dispatecherRef,
    long,
    lat,

    cb
  ) {
    var ok;
    await contract.contractInstance.methods
      .registerDispatcher(dispatcherAdd, dispatecherRef, long, lat)
      .send({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok);
        console.log(receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },

  //get meter firebase id from smart contract
  getFirebaseId: async function(add, cb) {
    await contract.contractInstance.methods
      .getDispatcherFireHashByAddress(add)
      .call({
        from: add,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      })
      .catch(err => {
        cb("error");
      });
  },
  //get all meters from DB
  getAll: async function(cb) {
    dispatcherDao.findAll(r => {
      cb(r);
    });
  },

  //get meter by id from DB
  getById: async function(id, cb) {
    dispatcherDao.findById(id, r => {
      cb(r);
    });
  },

  setIsworking: async function(id, val) {
    dispatcherDao.updateState(id, val);
  },

  setFirebaseId: async function(add, id) {
    await contract.contractInstance.methods
      .setFirebaseHashDispatcher(add, id)
      .send({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .on("error", error => {
        console.log(error);
      })
      .on("receipt", receipt => {
        console.log(receipt);
      })
      .catch(err => {
        console.log("error in setFirebaseId");
      });
  }
};
