const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const service = require("./service");
const control = require("./controller");

router.get("/test", (req, res) => {
  res.send("dispatcher test");
});

//---------------------------------------add dispatcher -------------------------------------

//service route : /adddispatcher
//parameters are sent in body
//parameters name :
// address : dispatcher etheureum address

//ref
//long : dispatcher position longitude
//lat :   dispatcher position latitude
//maxusers:
//radius

router.post(routing.ADD_DISPATCHER, async (req, res) => {
  var dispatcher = {
    address: "",
    ref: "",
    isWorking: "1",
    lat: "",
    long: "",
    maxusers: 100,
    radius: ""
  };
  dispatcher.address = req.body.address;

  dispatcher.ref = req.body.ref;
  dispatcher.long = req.body.long;
  dispatcher.lat = req.body.lat;
  dispatcher.maxusers = req.body.maxusers;
  dispatcher.radius = req.body.radius;

  control.generalRegestration(dispatcher, ok => {
    if (ok) {
      res.send("dispatcher registration done!!");
    } else {
      res.send("dispatcher exist!!");
    }
  });
});

//get firebase id by address from smart contract
//route /getDispatcherIdSc
//parameters are sent in the url not body
//example : http://localhost:3000/api/dispatcher/getDispatcherIdSc?add=0x365ca4bbca091d8b30ea9a79826f93f22245cedf
//parameters :
//add : dispatcher address
router.get(routing.GET_DISPATCHERFIREBASEID_SC, async (req, res) => {
  service.getFirebaseId(req.query.add, r => {
    if (r != "error") {
      res.send(r);
    } else {
      res.send("nothing");
    }
  });
});

//-------------------------get dispatcher by firebase id from DB-----------------------------------------
//service route /getbyid
//parameters send in url example :
//http://localhost:3000/api/meter/getbyid?dispatcherId=ffffff
//parameters :
//meterId : firebase id of the meter
router.get(routing.GET_DISPATCHERBYID, async (req, res) => {
  service.getById(req.query.dispatcherId, r => {
    res.send(r);
  });
});

//-------------------------get All dispatchers from DB-----------------------------------------
//service route /getdispatchers

//http://localhost:3000/api/meter/getdispatchers

router.get(routing.GET_DISPATCHERS, async (req, res) => {
  service.getAll(r => {
    res.send(r);
  });
});

//set is working
//service route : /setisworking
//parameters send in body
//id : dispatcher firebase id
//val : 0/1

router.post(routing.SET_ISWORKING, async (req, res) => {
  service.setIsworking(req.body.id, req.body.val);
  if (parseInt(req.body.val)) {
    res.send("dispatcher is started   ");
  } else {
    res.send("dispatcher is shutdown ");
  }
});

module.exports = router;
