const express = require("express");
const router = express.Router();
const routing = require("../../routes.json");

const control = require("./controller");
const service = require("./service");

router.get("/test", (req, res) => {
  res.send("testtessst");
});

//---------------------------------------mint token to  -------------------------------------
//service route : /mintto
//parameters send in body
//parameters name:
// amount : nomber of token to mint
// meterAdd : meter address to mint to
router.post(routing.MINTTO, async (req, res) => {
  try {
    service.mintTo(req.body.amount, req.body.meterAdd, ok => {
      if (ok) {
        res.send("mint succesfully !!!");
      } else {
        res.send("failed to mint path !!");
      }
    });
  } catch (e) {
    console.log(e);
    res.send(e);
  }
});
//---------------------------------------mint token   -------------------------------------
//service route : /mint
//parameters send in body
//parameters name:
// amount : nomber of token to mint
// meterAdd : meter address to mint to
router.post(routing.MINT, async (req, res) => {
  try {
    service.mintToken(req.body.meterAdd, req.body.amount, ok => {
      if (ok) {
        res.send("mint succesfully !!!");
      } else {
        res.send("failed to mint token !!");
      }
    });
  } catch (e) {
    console.log(e);
    res.send(e);
  }
});

//---------------------------------------burn token   -------------------------------------
//service route : /burn
//parameters send in body
//parameters name:
// amount : nomber of token to mint
// meterAdd : meter address to mint to
router.post(routing.BURN, async (req, res) => {
  try {
    service.burnToken(req.body.meterAdd, req.body.amount, ok => {
      if (ok) {
        res.send("burn succesfully !!!");
      } else {
        res.send("failed to burn token !!");
      }
    });
  } catch (e) {
    console.log(e);
    res.send(e);
  }
});

//---------------------------------------consume Energy -------------------------------------
//service route : /consumeEnergy
//parameters send in body
//parameters name:
// meterAddress : meter address that consume the energy
// ownerAddress : owner of the consumer meter
// amount : amount of BGToken
// announceId :firebase Id of the choosen announce by the user
router.post(routing.CONSUME, async (req, res) => {
  try {
    control.consumeToken(
      req.body.meterAddress,
      req.body.ownerAddress,
      req.body.amount,
      req.body.sellerMeterAddress,
      ok => {
        if (ok == "done") {
          res.send("consumming done");
        } else {
          if (ok == "insufficient money") {
            res.send("insufficient money");
          } else {
            res.send("failed");
          }
        }
      }
    );
  } catch (e) {
    console.log(e);
  }
});

module.exports = router;
