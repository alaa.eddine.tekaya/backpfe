const contract = require("../../config/web3Connexion");
const config = require("../../config/web3Config");
const service = require("../balance/service");
const userDao = require("../../dao/usersDao");
const meterDao = require("../../dao/meterDao");
const dispatcherDao = require("../../dao/dispatcherDao");
const pathDao = require("../../dao/pathDao");
module.exports = {
  mintTo: async function(amount, add, cb) {
    var ok;
    await contract.contractInstance.methods
      .mintTo(amount, add)
      .send({
        from: config.configWeb3.accAdress,
        gas: 500000
      })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok);
        console.log(receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },
  //------------------------------mint Token smart contract----------------------------------------//

  //parameters :
  //@meterAddress: address of the meter who is burning
  //@from_add : address of the sender of BGToken
  //@amount : amount of BGtoken

  mintToken: async function(meterAddress, amount, cb) {
    var ok;
    await contract.contractInstance.methods
      .mint(amount)
      .send({ from: meterAddress, gas: config.configWeb3.gaz })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok, receipt);
        console.log(receipt);
      })
      .catch(err => {
        ok = false;
        cb(ok);
        console.log(err);
      });
  },
  //------------------------------TransferToken smart contract----------------------------------------//

  //parameters :
  //@to_add : address of the reciever of BGtoken
  //@from_add : address of the sender of BGToken
  //@amount : amount of BGtoken

  transferToken: async function(to_add, amount, from_add, cb) {
    var ok;
    await contract.contractInstance.methods
      .transfer(to_add, amount)
      .send({ from: from_add, gas: config.configWeb3.gaz })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok);
        console.log(receipt);
      })
      .catch(err => {
        console.log(err);
      });
  },
  //------------------------------burnToken smart contract----------------------------------------//

  //parameters :
  //@meterAddress: address of the meter who is burning
  //@from_add : address of the sender of BGToken
  //@amount : amount of BGtoken

  burnToken: async function(meterAddress, amount, cb) {
    var ok;
    await contract.contractInstance.methods
      .burn(amount)
      .send({ from: meterAddress, gas: config.configWeb3.gaz })
      .on("error", error => {
        ok = false;
        cb(ok);
      })
      .on("receipt", receipt => {
        ok = true;
        cb(ok, receipt);
        console.log(receipt);
      })
      .catch(err => {
        ok = false;
        cb(ok);
        console.log(err);
      });
  },

  //when we get buy request , we add the price to the seller balance in the smart contract and the database
  //and the same we remove the price from buyer balance

  payEnergy: async function(buyer_address, seller_address, price, cb, cs, err) {
    try {
      service.removeMoney_sc(buyer_address, price, (r, reciept) => {
        if (r) {
          service.removeBalance(price, buyer_address);
          cb(reciept.transactionHash);
        } else {
          console.log("error while updating balance in smart contract");
        }
      });
      service.addMoney_sc(seller_address, price, (r, reciept) => {
        if (r) {
          service.AddBalance(price, seller_address);
          cs(reciept.transactionHash);
        } else {
          console.log("error while updating balance in smart contract");
        }
      });
    } catch (e) {
      err("payment error");
      console.log(e + "from buyenergy service");
    }
  },

  balanceOf: async function(add, cb) {
    await contract.contractInstance.methods
      .balanceOf(add)
      .call({
        from: config.configWeb3.accAdress,
        gas: config.configWeb3.gaz
      })
      .then(result => {
        console.log(result);
        cb(result);
      });
  },

  addTransaction: async function(useraddress, transaction) {
    userDao.findByAddress(useraddress, user => {
      var key = user[Object.keys(user)[0]].transactionCount + 1;

      userDao.addTransaction(Object.keys(user)[0], transaction, key);
      userDao.updateTransactionCount(Object.keys(user)[0], key);
    });
  },

  connexionIsActive: async function(meterAddress, num, isActive) {
    meterDao.findByAddress(meterAddress, meter => {
      var meterActive = meter[Object.keys(meter)[0]].isActive;

      var dispactcherId =
        meter[Object.keys(meter)[0]].connexions[num].dispatcher;
      var pathId = meter[Object.keys(meter)[0]].connexions[num].path;
      if (meterActive) {
        dispatcherDao.findById(dispactcherId, dispatcher => {
          if (dispatcher.isWorking) {
            pathDao.findById(pathId, path => {
              if (path.active) {
                isActive(true);

                console.log(
                  meterAddress + "  :" + "connexion" + num + " is active "
                );
              } else {
                console.log("path is down");
                isActive(false);
              }
            });
          } else {
            console.log("dispatcher is down");
            isActive(false);
          }
        });
      } else {
        console.log("meter: " + meterAddress + " is down");
        isActive(false);
      }
    });
  },
  activeConnexion: async function(meterAddress, cb) {
    meterDao.findByAddress(meterAddress, meter => {
      var connexionCount = meter[Object.keys(meter)[0]].connexionCount;

      for (let i = 1; i < connexionCount + 1; i++) {
        this.connexionIsActive(meterAddress, i, isactive => {
          if (isactive) {
            cb(true);
            return;
          }
        });
      }
      cb(false);
    });
  },
  metersCanCommunicate: async function(meterAddress1, meterAddress2, cb) {
    this.activeConnexion(meterAddress1, isactiv1 => {
      this.activeConnexion(meterAddress2, isactiv2 => {
        if (isactiv1 & isactiv2) {
          cb(true);
        } else {
          cb(false);
        }
      });
    });
  }
};
