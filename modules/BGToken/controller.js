const express = require("express");
const router = express.Router();

const service = require("./service");
const userDao = require("../../dao/usersDao");
const meterDao = require("../../dao/meterDao");
const sellAnnonces = require("../../dao/sellAnnoncesDao");

module.exports = {
  //when we get buy request , we add the price to the seller balance in the smart contract and the database
  //and the same we remove the price from buyer balance

  consumeToken: async function(
    meterAddress,
    ownerAddress,
    amount,
    sellerMeterAddress,
    cb
  ) {
    var buytransaction = {
      transctionHash: "",
      Date: "",
      amount: "",
      price: "",

      from: "",
      type: "buy"
    };
    var selltransaction = {
      transctionHash: "",
      Date: "",
      amount: "",
      price: "",

      to: "",
      type: "sell"
    };
    try {
      // meterDao.findById(sellerMeterId, meter => {
      //   var sellerAddress = meter.address;
      await service.metersCanCommunicate(
        meterAddress,
        sellerMeterAddress,
        isactive => {
          if (isactive) {
            userDao.findByAddress(ownerAddress, buyer => {
              var producer = buyer[Object.keys(buyer)[0]].producer;
              sellAnnonces.findByUser(producer, anounce => {
                if (anounce != null) {
                  var price =
                    parseInt(anounce[Object.keys(anounce)[0]].price) *
                    parseInt(amount);

                  if (buyer[Object.keys(buyer)[0]].balanceDt < price) {
                    cb("insufficient money");
                    return;
                  }
                  console.log(price);
                  // meterDao.findById(sellerMeterId, meter => {
                  //   var sellerAddress = meter.owner;

                  service.burnToken(
                    meterAddress,
                    parseInt(amount),
                    (ok, reciept) => {
                      if (ok) {
                        console.log(price);

                        service.balanceOf(meterAddress, r => {
                          console.log("balaaaaaaaaaaaaaaaaaaaaaaance:" + r);
                        });
                        service.payEnergy(
                          ownerAddress,
                          producer,
                          price,
                          buytransac => {
                            buytransaction.Date = Date.now();

                            buytransaction.from = producer;
                            buytransaction.price = price;
                            buytransaction.amount = amount;
                            buytransaction.transctionHash = buytransac;
                            //----------------------------------------------

                            service.addTransaction(
                              ownerAddress,
                              buytransaction
                            );
                          },
                          selltransac => {
                            selltransaction.Date = Date.now();

                            selltransaction.to = ownerAddress;
                            selltransaction.price = price;
                            selltransaction.amount = amount;
                            selltransaction.transctionHash = selltransac;
                            service.addTransaction(producer, selltransaction);
                            cb("done");
                          }
                        );
                      } else {
                        console.log("no more token to burn error");

                        return;
                      }
                    }
                  );
                  // });
                } else console.log("here");

                // return;
              });
            });
          } else {
          }
        }
      );
    } catch (e) {
      cb("error");
    }
  }
};
