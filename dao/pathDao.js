const db = require("../config/database");
var Path = db.defaultDatabase.ref("paths");

module.exports = {
  add: function(path, cb) {
    Path.push(path)
      .then(function(data) {
        console.log(data.getKey());
        cb(true);
      })
      .catch(function(error) {
        // alert(error);
        console.error(error);
        cb(false);
      });
  },
  setIsActive: function(pathId, val, cb) {
    db.defaultDatabase
      .ref("paths/" + pathId + "/active")
      .set(val)
      .then(function() {
        cb(true);
      })
      .catch(function(error) {
        cb(false);
        console.error(error);
      });
  },
  findAll: async function(cb) {
    await db.defaultDatabase.ref("paths/").once(
      "value",
      function(path) {
        console.log(path.val());
        cb(path.val());
      },
      function(errorObject) {
        cb("error");
        console.log("error from findall  :" + errorObject);
      }
    );
  },
  findById: async function(id, cb) {
    await db.defaultDatabase.ref("paths/" + id).once(
      "value",
      function(path) {
        cb(path.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  }
};
