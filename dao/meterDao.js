const db = require("../config/database");
var Meter = db.defaultDatabase.ref("meters");

module.exports = {
  add: function(meter) {
    Meter.push(meter)
      .then(function(data) {
        console.log(data.getKey());
      })
      .catch(function(error) {
        // alert(error);
        console.error(error);
      });
  },
  addConnexion: function(id, connexion, key) {
    db.defaultDatabase
      .ref("meters/" + id + "/connexions")
      .update({ [key]: connexion })
      .then(function(data) {})
      .catch(function(error) {
        console.error(error);
      });
  },
  setAddress: function(id, phaddress) {
    db.defaultDatabase
      .ref("meters/" + id + "/physicalAddress")
      .set(phaddress)
      .then(function(data) {})
      .catch(function(error) {
        console.error(error);
      });
  },
  updateConnexionCount: function(id, count) {
    db.defaultDatabase
      .ref("meters/" + id + "/connexionCount")
      .set(count)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  setOwner: function(id, newOwner) {
    db.defaultDatabase
      .ref("meters/" + id + "/owner")
      .set(newOwner)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  setIsActive: function(id, val) {
    db.defaultDatabase
      .ref("meters/" + id + "/isActive")
      .set(val)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  setRefFirebaseId: function(id, firebaseId) {
    db.defaultDatabase
      .ref("meters/" + id + "/RefFirebaseId")
      .set(firebaseId)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  updateRealTime: function(id, realtime) {
    db.defaultDatabase
      .ref("meters/" + id + "/realTime")
      .set(realtime)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },

  updateTotCon: function(id, totcons) {
    db.defaultDatabase
      .ref("meters/" + id + "/totalConsumption")
      .set(totcons)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  updateTotProd: function(id, totprod) {
    db.defaultDatabase
      .ref("meters/" + id + "/totalProduction")
      .set(totprod)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  Delete: function(id) {
    db.defaultDatabase
      .ref("meters/" + id)
      .remove()
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  DeleteConnexion: function(id, key, cb) {
    db.defaultDatabase
      .ref("meters/" + id + "/connexions/" + key)
      .remove()
      .then(function() {
        cb(true);
      })
      .catch(function(error) {
        cb(false);
        console.error(error);
      });
  },
  findById: async function(id, cb) {
    await db.defaultDatabase.ref("meters/" + id).once(
      "value",
      function(meter) {
        // console.log(meter.val());
        cb(meter.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  },
  findByUser: async function(userId, cb) {
    await db.defaultDatabase
      .ref("meters/")
      .orderByChild("owner")
      .equalTo(userId)
      .once(
        "value",
        function(meter) {
          // console.log("meter " + meter.val());
          cb(meter.val());
        },
        function(errorObject) {
          console.log("error from meter by user :" + errorObject);
        }
      );
  },
  findByAddress: async function(add, cb) {
    await db.defaultDatabase
      .ref("meters/")
      .orderByChild("address")
      .equalTo(add)
      .once(
        "value",
        function(meter) {
          //console.log("meter from find by add" + meter.val());
          cb(meter.val());
        },
        function(errorObject) {
          console.log("error from meter by address :" + errorObject);
        }
      );
  },
  findAll: async function(cb) {
    await db.defaultDatabase.ref("meters/").once(
      "value",
      function(meter) {
        console.log(meter.val());
        cb(meter.val());
      },
      function(errorObject) {
        console.log("error");
        console.log("error from by id :" + errorObject);
      }
    );
  }
};
