const db = require("../config/database");
var userBank = db.defaultDatabase.ref("UsersBank");

module.exports = {
  addCard: function(userAddress, card, cb) {
    userBank
      .update({ [userAddress]: card })
      .then(function(data) {
        cb(true);
      })
      .catch(function(error) {
        cb(false);
        console.error(error);
      });
  },

  findByAddress: async function(address, cb) {
    await db.defaultDatabase.ref("UsersBank/" + address).once(
      "value",
      function(card) {
        console.log(card.val());
        cb(card.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  },
  updateBalance: function(address, balance) {
    db.defaultDatabase
      .ref("UsersBank/" + address + "/balanceDt")
      .set(balance)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  }
};
