const db = require("../config/database");
var Users = db.defaultDatabase.ref("users");

module.exports = {
  add: function(user, cb) {
    Users.push(user)
      .then(function(data) {
        //console.log(data.getKey());
        cb(data.getKey());
      })
      .catch(function(error) {
        // alert(error);
        console.error(error);
      });
  },
  addMeter: function(id, meterRference, cb) {
    db.defaultDatabase
      .ref("users/" + id + "/meterReferences")
      .push({ ref: meterRference })
      .then(function(data) {
        cb(data.getKey());
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  addTransaction: function(id, transaction, key) {
    db.defaultDatabase
      .ref("users/" + id + "/transactions")
      .update({ [key]: transaction })
      .then(function(data) {})
      .catch(function(error) {
        console.error(error);
      });
  },

  updatePhAddress: function(id, address) {
    db.defaultDatabase
      .ref("users/" + id + "/physicalAddress")
      .set(address)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  updateTransactionCount: function(id, count) {
    db.defaultDatabase
      .ref("users/" + id + "/transactionCount")
      .set(count)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },

  updateName: function(id, name) {
    db.defaultDatabase
      .ref("users/" + id + "/username")
      .set(name)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  updateProducer: function(id, producer) {
    db.defaultDatabase
      .ref("users/" + id + "/producer")
      .set(producer)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  updateRate: function(id, rate) {
    db.defaultDatabase
      .ref("users/" + id + "/rating")
      .set(rate)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  updateBalance: function(id, balance) {
    db.defaultDatabase
      .ref("users/" + id + "/balanceDt")
      .set(balance)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  Delete: function(id) {
    db.defaultDatabase
      .ref("users/" + id)
      .remove()
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  deleteByMeter: async function(userid, meterid) {
    db.defaultDatabase
      .ref("users/" + userid + "/meterReferences/")
      .child(meterid)
      .remove()
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  findById: async function(id, cb) {
    await db.defaultDatabase.ref("users/" + id).on(
      "value",
      function(user) {
        console.log(user.val());
        cb(user.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  },
  findByAddress: async function(add, cb) {
    await db.defaultDatabase
      .ref("users/")
      .orderByChild("walletAddress")
      .equalTo(add)
      .once(
        "value",
        function(user) {
          console.log("user " + user.val());
          cb(user.val());
        },
        function(errorObject) {
          console.log("error from user by address :" + errorObject);
        }
      );
  },
  findByMeterId: async function(id, cb) {
    await db.defaultDatabase
      .ref("users/")

      .on(
        "value",
        function(user) {
          console.log("user " + user.val());
          cb(user.val());
        },
        function(errorObject) {
          console.log("error from user by address :" + errorObject);
        }
      );
  },
  findAll: function(cb) {
    db.defaultDatabase.ref("users/").once(
      "value",
      function(user) {
        console.log(user.val());
        cb(user.val());
        return user.val();
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  }
};
