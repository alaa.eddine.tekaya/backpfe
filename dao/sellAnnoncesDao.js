const db = require("../config/database");
var SellAnnonces = db.defaultDatabase.ref("SellAnnonces");

module.exports = {
  add: function(annonce, cb) {
    var ok;
    SellAnnonces.push(annonce)
      .then(function(data) {
        console.log(data.getKey());
        ok = true;
        cb(ok);
        return data.getKey();
      })
      .catch(function(error) {
        ok = false;
        cb(ok);
        // alert(error);
        console.error(error);
      });
  },
  updateState: function(id, stat, cb) {
    db.defaultDatabase
      .ref("SellAnnonces/" + id + "/stat")
      .set(stat)
      .then(function() {
        cb(true);
      })
      .catch(function(error) {
        cb("error");
        console.error(error);
      });
  },

  Delete: function(id, cb) {
    db.defaultDatabase
      .ref("SellAnnonces/" + id)
      .remove()
      .then(function() {
        cb(true);
      })
      .catch(function(error) {
        cb("error");
        console.error(error);
      });
  },
  findById: async function(id, cb) {
    await db.defaultDatabase.ref("SellAnnonces/" + id).once(
      "value",
      function(annonce) {
        console.log(annonce.val());
        cb(annonce.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  },
  findAll: async function(cb) {
    await db.defaultDatabase.ref("SellAnnonces/").once(
      "value",
      function(annonce) {
        console.log(annonce.val());
        cb(annonce.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  },
  findByMeter: async function(meter, cb) {
    await db.defaultDatabase
      .ref("SellAnnonces/")
      .orderByChild("meterAddress")
      .equalTo(meter)
      .once(
        "value",
        function(annonce) {
          console.log(annonce.val());
          cb(annonce.val());
        },
        function(errorObject) {
          cb("error");
          console.log("error from by id :" + errorObject);
        }
      );
  },
  findByUser: async function(userAddress, cb) {
    await db.defaultDatabase
      .ref("SellAnnonces/")
      .orderByChild("owner")
      .equalTo(userAddress)
      .once(
        "value",
        function(annonce) {
          console.log(annonce.val());
          cb(annonce.val());
        },
        function(errorObject) {
          cb("error");
          console.log("error from by id :" + errorObject);
        }
      );
  },
  findByEndDate: async function(date, cb) {
    await db.defaultDatabase
      .ref("SellAnnonces/")
      .orderByChild("endDate")
      .equalTo(date)
      .once(
        "value",
        function(annonce) {
          console.log(annonce.val());
          cb(annonce.val());
        },
        function(errorObject) {
          console.log("error from by id :" + errorObject);
        }
      );
  }
};
