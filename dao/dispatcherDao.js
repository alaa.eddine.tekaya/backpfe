const db = require("../config/database");
var Dispatcher = db.defaultDatabase.ref("dispatchers");

module.exports = {
  add: function(dispatcher) {
    Dispatcher.push(dispatcher)
      .then(function(data) {
        console.log(data.getKey());
        return data.getKey();
      })
      .catch(function(error) {
        // alert(error);
        console.error(error);
      });
  },
  updateState: function(id, stat) {
    db.defaultDatabase
      .ref("dispatchers/" + id + "/isWorking")
      .set(stat)
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },

  Delete: function(id) {
    db.defaultDatabase
      .ref("dispatchers/" + id)
      .remove()
      .then(function() {
        return true;
      })
      .catch(function(error) {
        console.error(error);
      });
  },
  findById: async function(id, cb) {
    await db.defaultDatabase.ref("dispatchers/" + id).once(
      "value",
      function(dispatcher) {
        cb(dispatcher.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  },
  findByAddress: async function(add, cb) {
    await db.defaultDatabase
      .ref("dispatchers/")
      .orderByChild("address")
      .equalTo(add)
      .once(
        "value",
        function(dispatcher) {
          console.log("dispatcher from find by add" + dispatcher.val());
          cb(dispatcher.val());
        },
        function(errorObject) {
          console.log("error from dispatcher by address :" + errorObject);
        }
      );
  },
  findAll: async function(cb) {
    await db.defaultDatabase.ref("dispatchers/").once(
      "value",
      function(dispatcher) {
        console.log(dispatcher.val());
        cb(dispatcher.val());
      },
      function(errorObject) {
        console.log("error from by id :" + errorObject);
      }
    );
  }
};
