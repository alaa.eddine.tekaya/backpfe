const db = require("../config/database");
const userDao = require("../dao/usersDao");
module.exports = {
  realTimeData: async function(io) {
    io.sockets.on("connection", async function(socket) {
      var id;
      await socket.on("meter", async function(meterFirebaseId) {
        console.log(meterFirebaseId);
        db.defaultDatabase
          .ref("meters/" + meterFirebaseId + "/realTime")
          .on("child_changed", function(snapshot) {
            var changedRealTime = {};
            changedRealTime[snapshot.key] = snapshot.val();
            console.log(changedRealTime);
            socket.emit("real_time", changedRealTime);
          });
      });
    });
  },
  balanceDt: async function(io) {
    io.sockets.on("connection", async function(socket) {
      var id;
      await socket.on("user", async function(userFirebaseId) {
        console.log(userFirebaseId);
        db.defaultDatabase
          .ref("users/" + userFirebaseId)
          .on("child_changed", function(snapshot) {
            var changedBalance = {};
            changedBalance[snapshot.key] = snapshot.val();
            console.log(changedBalance);
            socket.emit("balance", changedBalance);
          });
      });
    });
  },
  TransactionRt: async function(io) {
    io.sockets.on("connection", async function(socket) {
      var id;
      await socket.on("user", async function(userId) {
        console.log(userId);

        db.defaultDatabase
          .ref("users/" + userId + "/transactions")
          .on("child_changed", function(snapshot) {
            var newTransaction = snapshot.val();

            console.log(newTransaction);
            socket.emit("transaction", newTransaction);
          });
        db.defaultDatabase
          .ref("users/" + userId + "/transactionCount")
          .on("child_changed", function(snapshot) {
            var transactionCount = snapshot.val();

            console.log(transactionCount);
            socket.emit("transactionCount", transactionCount);
          });
      });
    });
  },
  TransactionsAdmin: async function(io) {
    io.sockets.on("connection", async function(socket) {
      var id;
      await socket.on("admin", async function() {
        db.defaultDatabase.ref("users/").on("child_added", function(snapshot) {
          var newTransaction = snapshot.val();

          console.log(newTransaction);
          socket.emit("transaction", newTransaction);
        });
      });
    });
  }
};
