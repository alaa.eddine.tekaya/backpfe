const express = require("express");
const app = express();
var server = require("http").createServer(app);
const cors = require("cors");
const bodyParser = require("body-parser");
// const axios = require("axios");

// axios({
//   method: "get",
//   url:
//     "https://nominatim.openstreetmap.org/reverse?format=json&lat=" +
//     long +
//     "&lon=" +
//     lat +
//     "&zoom=18&addressdetails=1",
//   responseType: "json"
// }).then(function(response) {
//   console.log(response.data.address);
// });

//--------------------------------------------------Partie realTime communication----------------------------------------------
//to get consumption and production you need in front to emit event called meter with meterFirebaseId
//like this ---> socket.emit("meter",meterFirebaseId)
// and a listener on event called "real-time"  that return the changed value {consum or prod : new val}
//like this ---> socket.on("real-time",function(data){ your actions })

//same to get changed balance
//like this ---> socket.emit("user",userFirebaseId)
// and a listener on event called "balance"  that return the changed value {balanceDt : new val}
//like this ---> socket.on("balance",function(data){ your actions })
const rt = require("./realTime/rt");
var io = require("socket.io").listen(server);
rt.realTimeData(io);
rt.balanceDt(io);
rt.TransactionRt(io);
rt.TransactionsAdmin(io);

//--------------------------------------------------------------------------------------------

// load routes files

var routes = require("./modules/routes.js");

var service = require("./modules/BGToken/service");
const userDao = require("./dao/usersDao");
// service.connexionIsActive(
//   "0x14af2793ffbcd55411834119a79d5c94eb0d30f3",
//   1,
//   isActive => {
//     console.log(isActive);
//   }
// );

// CORS initialization
app.use(cors());

app.use(bodyParser.json());
// Routes
app.use("/api/", routes);
app.get("/email", (req, res) => {
  res.send("its oki ");
});

server.listen(3000, () => {
  console.log("backend listen on port " + 3000);
});
